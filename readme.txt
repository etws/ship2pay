== Short Description
Module that limits the number of payment options depending on the chosen shipping method. 
For example When my client chooses pickup at store he should not be able to select bank transfer.

== LICENSE
This extension is developed by Pawel Krzaczkowski (pawel@freshmind.pl)

ET Web Solutions Team have modified extension code for better UI.
support@etwebsolutions.com


== Modifications
see changelog.txt


== Extension permanent link
Original: http://www.magentocommerce.com/magento-connect/ship-and-pay-combo.html
Modified: https://bitbucket.org/etws/ship2pay


== Version Compatibility
Magento CE:
1.9.x (tested in 1.9.2.1)


== Installation
* Disable compilation if it is enabled (System -> Tools -> Compilation)
* Disable cache if it is enabled (System -> Cache Management)
* Download the extension or install the extension from Magento Connect
* If you have downloaded it, copy all files from the "install" folder to the Magento root folder - where your index.php is
* Log out from the admin panel
* Log in to the admin panel with your login and password
* Set extension's parameters (System -> Configuration -> Shipping Settings -> Ship2Pay)
* Run the compilation process and enable cache if needed

