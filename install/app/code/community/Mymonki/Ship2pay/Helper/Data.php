<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mymonki
 * @package    Mymonki_Ship2pay
 * @copyright  Copyright (c) 2010 Freshmind Sp. z o.o. (pawel@freshmind.pl)
 */
class Mymonki_Ship2pay_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_PATH_PAYMENT_METHODS = 'payment';

    public function getPaymentMethodOptions($storeId = null)
    {

        $methods = Mage::helper('payment')->getPaymentMethods($storeId);
        $options = array();

        foreach ($methods as $code => $methodConfig) {
            $prefix = self::XML_PATH_PAYMENT_METHODS . '/' . $code . '/';
            if (!$model = Mage::getStoreConfig($prefix . 'model', $storeId)) {
                unset($methods[$code]);
                continue;
            }
            $methodInstance = Mage::getModel($model);

            $methodLabel = $methodInstance->getConfigData('title');
            $methodLabel = trim($methodLabel);

            if (!$methodLabel) {
                $methodLabel = $methodInstance->getCode();
            }


            $methodClass = get_class($methodInstance);
            $moduleName = $this->getModuleNameByClass($methodClass);

            $isActive = Mage::getStoreConfig("payment/" . $methodInstance->getCode() . "/active");
            if ($isActive) {
                $isActive = $this->__('Active');
            } else {
                $isActive = $this->__('Not Active');
            }
            $methods[$code]['is_active'] = $isActive;

            array_unshift($options, array(
                'value' => $code,
                'label' => $methodLabel . ' / ' . $isActive . ' (' . $moduleName . ')',
            ));
        }
        uasort($options, array($this, 'compareOptionLabel'));

        return $options;
    }

    public function getShipingMethodOptions($storeId = null)
    {
        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers($storeId);
        $options = array();

        foreach ($carriers as $carrierCode => $carrierConfig) {
            $carrierClass = get_class($carrierConfig);
            $moduleName = $this->getModuleNameByClass($carrierClass);

            $isActive = Mage::getStoreConfig("carriers/$carrierCode/active");
            if ($isActive) {
                $isActive = $this->__('Active');
            } else {
                $isActive = $this->__('Not Active');
            }

            try {
                if (version_compare(Mage::getVersion(), '1.5.0', '>=')) {
                    $methods = $carrierConfig->getAllowedMethods();
                    if ($methods) {
                        if (count($methods) > 1) {
                            foreach ($methods as $method => $methodName) {
                                if (!is_array($methodName)) {
                                    if (preg_match('#^([a-zA-Z0-9])+$#', $method)) {
                                        array_unshift($options, array(
                                            'value' => $carrierCode . '_' . $method,
                                            'label' => $this->getCarrierName($carrierCode) . ' - ' . $methodName . ' / ' . $isActive . ' (' . $moduleName . ')'
                                        ));
                                    }
                                }
                            }
                        }
                    }
                }

                array_unshift($options, array(
                    'value' => $carrierCode,
                    'label' => $this->getCarrierName($carrierCode) . ' / ' . $isActive . ' (' . $moduleName . ')'
                ));

            } catch (Exception $e) {

            }

        }

        uasort($options, array($this, 'compareOptionLabel'));
        return $options;
    }

    public function getModuleNameByClass($class)
    {
        $moduleNameTmp = '';
        if (Mage::getConfig()->getNode('modules/' . $class)) {
            //$methods[$code]['module_name'] = $moduleNameTmp;
            return $class;
        } else {
            $moduleNameArray = explode('_', $class);
            $moduleNameArraySize = count($moduleNameArray);
            for ($i = 0; $i <= $moduleNameArraySize; $i++) {
                $moduleNameTmp = implode('_', $moduleNameArray);
                if (Mage::getConfig()->getNode('modules/' . $moduleNameTmp)) {
                    return $moduleNameTmp;
                } else {
                    array_pop($moduleNameArray);
                }
            }
        }

        return $moduleNameTmp;
    }

    public function compareOptionLabel($a, $b)
    {
        return strnatcasecmp($a['label'], $b['label']);
    }

    public function getCarrierName($carrierCode)
    {
        if ($name = Mage::getStoreConfig('carriers/' . $carrierCode . '/title')) {
            return $name;
        }
        return $carrierCode;
    }

}