<?php

class Mymonki_Ship2pay_Model_Observer extends Varien_Object
{
    public function sortShip2PayConfiguration($observer)
    {
        $ship2payConfiguration = unserialize(Mage::getStoreConfig('shipping/ship2pay/ship2pay'));

        $newArray = array();
        foreach ($ship2payConfiguration['shipping_method'] as $key => $method) {
            $newArray[] = array(
                'shipping_method' => $method,
                'payment_method' => $ship2payConfiguration['payment_method'][$key]
            );
        }

        uasort($newArray, array($this, 'compareShippingMethod'));
        $ship2payConfiguration = array();
        $counter = 1;
        foreach ($newArray as $combination) {
            $ship2payConfiguration['shipping_method'][$counter] = $combination['shipping_method'];
            $ship2payConfiguration['payment_method'][$counter] = $combination['payment_method'];
            $counter++;
        }

        $config = new Mage_Core_Model_Config();
        $config->saveConfig('shipping/ship2pay/ship2pay', serialize($ship2payConfiguration), 'default', 0);
    }


    public function compareShippingMethod($a, $b)
    {
        return strnatcasecmp($a['shipping_method'], $b['shipping_method']);
    }
}